/**
* @file     estatisticas.h
* @brief    Header com as assinaturas das funções que calculam estatísticas
* @author   Airton Neto
* @since    12/04/2017
* @date     13/04/2017
* @sa       struct.h
*/
#ifndef ESTATISTICAS_H
#define ESTATISTICAS_H

#include"struct.h"

/**
* @brief 			Função geradora da estatistica do maior número de nascidos por ano
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param Maiorano	Vetor que armazena a posição das cidade com o maior número de nascidos daquele ano
* @param contador	Número de linhas do arquivo
* @return 			Sem retorno
*/
void maiorano(Stats *test, int *Maiorano, int contador);
/**
* @brief 			Função geradora da estatistica do menor número de nascidos por ano
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param Menorano	Vetor que armazena a posição das cidade com o menor número de nascidos daquele ano
* @param contador	Número de linhas do arquivo
* @return 			Sem retorno
*/
void menorano(Stats *test, int *Menorano, int contador);
/**
* @brief 			Função geradora da estatistica do total de nascidos por ano
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param Totalano	Vetor que armazena o total de nascidos por ano
* @param contador	Número de linhas do arquivo
* @return 			Sem retorno
*/
void totalano(Stats *test, float *Totalano, int contador);
/**
* @brief 			Função geradora da estatistica da média de nascidos por ano
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param Mediaano   Vetor que armazenará a média de nascidos por ano
* @param contador	Número de linhas do arquivo
* @param Totalano	Vetor que armazenou o total de nascidos por ano
* @return 			Sem retorno
*/
void mediaano(Stats *test, float *Mediaano, int contador, float *Totalano);
/**
* @brief 			Função geradora da estatistica do total de nascidos por ano
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param Mediaano   Vetor que armazenou a média de nascidos por ano
* @param contador	Número de linhas do arquivo
* @param Desvioano	Vetor que armazenará o desvio padrão por ano
* @return 			Sem retorno
*/
void desvioano(Stats *test, float *Mediaano, int contador, float *Desvioano);

#endif