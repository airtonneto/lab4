/**
* @file     tarefa2.h
* @brief    Header com as assinaturas das funções da Tarefa 2
* @author   Airton Neto
* @since    12/04/2017
* @date     13/04/2017
* @sa       struct.h
*/
#ifndef TAREFA_H
#define TAREFA_H

#include"struct.h"
/**
* @brief			Função que calculará e imprime a taxa de queda de nascimentos de 2014 em relação a 2013
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param contador   Número de linhas do arquivo
* @return 			Sem retorno
*/
void taxaqueda(Stats *test, int contador);
/**
* @brief			Função que calculará e imprime a taxa de crescimento de nascimentos de 2014 em relação a 2013
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param contador   Número de linhas do arquivo
* @return 			Sem retorno
*/
void taxacresce(Stats *test, int contador);

#endif