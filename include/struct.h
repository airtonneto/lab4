/**
* @file 	struct.h
* @brief 	Struct que armazena os dados dos nascimentos de um município por ano
* @author 	Airton Neto
* @since 	12/04/2017
* @date	        13/04/2017
*/
#ifndef STRUCT_H
#define STRUCT_H
  

#include<string>
using std::string;

/**
* @struct       Stats struct.h
* @brief        Tipo estrutura que agrega os dados de nascimento de um municipio
* @details      Os dados cobrem os anos de 1994 a 2014
*/
struct Stats{
        string codigo;  /**< Codigo do municipio*/
        string nome;    /**< Nome do municipio*/
        int nascimentos[21];    /**< Numero de nascimentos em cada ano contabilizado*/
};

#endif