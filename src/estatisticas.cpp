/**
* @file     estatisticas.cpp
* @brief    Implementação das funções que calculam as estatísticas por ano
* @author   Airton Neto
* @since    06/04/2017
* @date     13/04/2017
* @sa       estatisticas.h
* @sa       struct.h
*/
#include "estatisticas.h"
#include"struct.h"
#include<cmath>
#include<iostream>
using std::cout;
using std::endl;

/**
* @brief 			Função geradora da estatistica do maior número de nascidos por ano
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param Maiorano	Vetor que armazena a posição das cidade com o maior número de nascidos daquele ano
* @param contador	Número de linhas do arquivo
* @return 			Sem retorno
*/
void maiorano(Stats *test, int *Maiorano, int contador){
    int auxvalor=0;
    for(int mm=0; mm<21; mm++){
        Maiorano[mm]=0;
        auxvalor=0;
        for(int pp=0; pp<contador-2; pp++){
            if(auxvalor<= test[pp].nascimentos[mm]){
                auxvalor= test[pp].nascimentos[mm];
                Maiorano[mm]= pp;
            }
        }
    }
}
/**
* @brief 			Função geradora da estatistica do menor número de nascidos por ano
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param Menorano	Vetor que armazena a posição das cidade com o menor número de nascidos daquele ano
* @param contador	Número de linhas do arquivo
* @return 			Sem retorno
*/
void menorano(Stats *test, int *Menorano, int contador){
    int auxvalor;
    for(int mm=0; mm<21; mm++){
        Menorano[mm]=0;
        auxvalor=test[0].nascimentos[mm];
        for(int pp=0; pp<contador-2; pp++){
            if(auxvalor>= test[pp].nascimentos[mm]){
                auxvalor= test[pp].nascimentos[mm];
                Menorano[mm]= pp;
            }
        }
    }
}

/**
* @brief 			Função geradora da estatistica do total de nascidos por ano
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param Totalano	Vetor que armazena o total de nascidos por ano
* @param contador	Número de linhas do arquivo
* @return 			Sem retorno
*/
void totalano(Stats *test, float *Totalano, int contador){
    for(int mm=0; mm<21; mm++){
        Totalano[mm]=0;
        for(int pp=0; pp<contador-2; pp++){
            Totalano[mm]=Totalano[mm] +test[pp].nascimentos[mm];
        }
    }
}
/**
* @brief 			Função geradora da estatistica da média de nascidos por ano
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param Mediaano   Vetor que armazenará a média de nascidos por ano
* @param contador	Número de linhas do arquivo
* @param Totalano	Vetor que armazenou o total de nascidos por ano
* @return 			Sem retorno
*/
void mediaano(Stats *test, float *Mediaano, int contador, float *Totalano){
    for(int mm=0; mm<21; mm++){
        Mediaano[mm]= Totalano[mm]/ (contador-2);
    }
}

/**
* @brief 			Função geradora da estatistica do total de nascidos por ano
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param Mediaano   Vetor que armazenou a média de nascidos por ano
* @param contador	Número de linhas do arquivo
* @param Desvioano	Vetor que armazenará o desvio padrão por ano
* @return 			Sem retorno
*/
void desvioano(Stats *test, float *Mediaano, int contador, float *Desvioano){
        float aux=0;
        for(int mm=0; mm<21; mm++){
            aux=0;
            for(int pp=0; pp<contador-2; pp++){
                aux=aux+ ((test[pp].nascimentos[mm])-Mediaano[mm])*((test[pp].nascimentos[mm])-Mediaano[mm]);
            }
            Desvioano[mm]= sqrt(aux/ (contador-2));
    }
}