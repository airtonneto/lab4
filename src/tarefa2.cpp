/**
* @file     tarefa2.cpp
* @brief    Implementação das funções que calculam as estatísticas da Tarefa 2
* @author   Airton Neto
* @since    12/04/2017
* @date     13/04/2017
* @sa       tarefa2.h
* @sa       struct.h
*/
#include"tarefa2.h"
#include"struct.h"

#include<iostream>
using std::cout;
using std::endl;

#include<string>
using std::string;

/**
* @brief			Função que calculará e imprime a taxa de queda de nascimentos de 2014 em relação a 2013
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param contador   Número de linhas do arquivo
* @return 			Sem retorno
*/
void taxaqueda(Stats *test, int contador){
    cout<<endl<<endl;
    float aux=0, Taxaqueda, aux1, aux2, T1, T2;
    int posTaxaQ;
    Taxaqueda=(test[0].nascimentos[20])/(test[0].nascimentos[19]);
    for(int pp=0; pp<contador-2; pp++){
        aux1=(test[pp].nascimentos[20]);
        aux2=(test[pp].nascimentos[19]);
        aux=aux1/aux2;
        if(Taxaqueda>aux){
            T1=(test[pp].nascimentos[20]);
            T2=(test[pp].nascimentos[19]);
            Taxaqueda=T1/T2;
            posTaxaQ=pp;
        }
    }
    cout<<"Municipio com maior taxa de queda 2013-2014: "<<test[posTaxaQ].nome
        <<" ("<<100*(Taxaqueda-1)<<"%)"<<endl;
}


/**
* @brief			Função que calculará e imprime a taxa de crescimento de nascimentos de 2014 em relação a 2013
* @param test		Struct do tipo Stats que contem nome, codigo e numero de nascimentos
* @param contador   Número de linhas do arquivo
* @return 			Sem retorno
*/
void taxacresce(Stats *test, int contador){
    float aux=0, Taxacresce, aux1, aux2, T1, T2;
    int posTaxaC;
    Taxacresce=(test[0].nascimentos[20])/(test[0].nascimentos[19]);
    for(int pp=0; pp<contador-2; pp++){
        aux1=(test[pp].nascimentos[20]);
        aux2=(test[pp].nascimentos[19]);
        aux=aux1/aux2;
        if(Taxacresce<aux){
            T1=(test[pp].nascimentos[20]);
            T2=(test[pp].nascimentos[19]);
            Taxacresce=T1/T2;
            posTaxaC=pp;
        }
    }
    cout<<"Municipio com maior taxa de crescimento 2013-2014: "<<test[posTaxaC].nome
        <<" ("<<100*(Taxacresce-1)<<"%)"<<endl;
}