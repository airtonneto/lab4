/**
* @file     main.cpp
* @brief    Função Principal
* @author   Airton Neto
* @since    06/04/2017
* @date     13/04/2017
* @sa       estatisticas.h
* @sa       tarefa2.h
* @sa       struct.h
*/
#include<fstream>
using std::ifstream;
using std::ofstream;

#include<iostream>
using std::cerr;
using std::cout;
using std::endl;

#include<string>
using std::string;

#include<cstdlib>

#include"estatisticas.h"
#include"tarefa2.h"

#include"struct.h"

int main(int argc, char *argv[]){

    //Ler o arquivo para saber a quantidade de cidades
    int contador=0;
    ifstream tamanho(argv[1]);
    if(!tamanho){
        cout<<"O arquivo de entrada nao pode ser aberto."<<endl;
        return 0;
    }
    string contadora;
    while(getline(tamanho, contadora)){
        ++contador;
    }
    
    
    tamanho.seekg(0);
    tamanho.close();
    
    //Ler o arquivo para armazenar os dados na struct
    ifstream entrada(argv[1]);
    
    if(!entrada){
        cout<<"O arquivo de entrada nao pode ser aberto."<<endl;
        return 0;
    }
    string nascimentos[21];
    
    Stats *test= new Stats[contador-1];
    
    string *auxiliar= new string[contador-1];
    int soma=0, totallinha=0;
    //Ignora primeira linha
    string null;
    getline (entrada, null);
    //Armazenamento na Struct
    for(int i=0; i<contador-2; i++){
        if(entrada.eof()){
            break;
        }
        getline (entrada, test[i].codigo, ' ');
        getline (entrada, test[i].nome, ';');
        for(int j=0; j<21; j++){
            getline (entrada, nascimentos[j], ';');
            if(nascimentos[j]=="-"){
                nascimentos[j]="0";
            }
            test[i].nascimentos[j]= atoi(nascimentos[j].c_str());
        }
        getline (entrada, auxiliar[i]);
    }
    //Conferir se o total é igual à soma
    for(int ll=0; ll<contador-2; ll++){
        totallinha= atoi(auxiliar[ll].c_str());
        for(int zz=0; zz<21; zz++){
            soma=soma+ test[ll].nascimentos[zz];
        }
        if(soma!=totallinha){
            cout<<"linha: "<<ll<<" total por cidade não compatível com tabela"<<endl;
        }
        soma=0;
    }
    delete [] auxiliar;
    //Chamada das funções das estatisticas
    int *Maiorano= new int[21];
    int *Menorano= new int[21];
    float *Totalano= new float[21];
    float *Mediaano= new float[21];
    float *Desvioano= new float[21];
    maiorano(test, Maiorano, contador);
    menorano(test, Menorano, contador);
    totalano(test, Totalano, contador);
    mediaano(test, Mediaano, contador, Totalano);
    desvioano(test, Mediaano, contador, Desvioano);

    entrada.close();

    //Armazenar as estatisticas no estatisticas.csv
    ofstream saida("data/estatisticas.csv");
    if(!saida){
        cout<<"O arquivo de entrada nao pode ser aberto."<<endl;
        return 0;
    }
    for(int i=0; i<21; i++){
        saida<<i+1994<<";";
        saida<<test[ Maiorano[i] ].nascimentos[i]<<";";
        saida<<test[ Menorano[i] ].nascimentos[i]<<";";
        saida<<Mediaano[i]<<";";
        saida<<Desvioano[i]<<";";
        saida<<Totalano[i]<<";";
        saida<<endl;
    }
    saida.close();
    //Armazenar os dados no totais.dat
    ofstream saida2("data/totais.dat");
    if(!saida2){
        cout<<"O arquivo de entrada nao pode ser aberto."<<endl;
        return 0;
    }
    for(int i=0; i<21; i++){
        saida2<<i+1994<<" ";
        saida2<<Totalano[i];
        saida2<<endl;
    }
    saida2.close();
    taxaqueda(test, contador);
    taxacresce(test, contador);
    delete [] test;
    return 0;
}
